# Change Log

## 0.3.0

### Minor Changes

- [#146](https://github.com/rx-ts/react/pull/146) [`d151662`](https://github.com/rx-ts/react/commit/d151662c761589ec575fe6be773b2894ae502745) Thanks [@JounQin](https://github.com/JounQin)! - feat: migrate react-rx package

## 0.2.0

### Minor Changes

- [#144](https://github.com/rx-ts/react/pull/144) [`bc5a7ea`](https://github.com/rx-ts/react/commit/bc5a7ea465801b6aa82528483f2c440d86d4147c) Thanks [@JounQin](https://github.com/JounQin)! - feat: migrate react-storage package

## 0.1.0

### Minor Changes

- [#142](https://github.com/rx-ts/react/pull/142) [`a028935`](https://github.com/rx-ts/react/commit/a028935bbbb2c937b8ae7cdef8a91e0c437eb179) Thanks [@JounQin](https://github.com/JounQin)! - feat: first blood, should just work
